﻿/* -------------------------------------------------------------------- */
/* Bluetooth Optical Probe Library										*/
/* -------------------------------------------------------------------- */
/* Developed by "Mobit Bilisim Elektronik Ve Kontrol Sistemleri Dis		*/
/* Tic. A.S.". Please read license.txt for the information and legal	*/
/* notices.																*/
/* -------------------------------------------------------------------- */

using System;
using System.Threading;
using Windows.Foundation;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.Networking.Sockets;
using Windows.Devices.Enumeration;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Devices.SerialCommunication;
using System.Runtime.InteropServices.WindowsRuntime;

namespace MobitBtProbeUwpDemo
{
	class MobitBtProbe : IDisposable
	{
		enum PACKET_CLASS
		{
			EVENT,
			COMMAND,
			RESULT
		};

		enum PROBE_EVENT
		{
			POWER,
			TRIGGER
		};

		enum PROBE_COMMAND
		{
			GETVERSION,
			GETBTVERSION,
			POWEROFF,
			SETLED,
			GETPOWERSTATUS,
			IRENABLE,
			IRSETBAUDRATE
		};

		enum PROBE_RESULT
		{
			SUCCESS,
			FAILED,
			NOTIMPLEMENTED
		};

		public enum IR_BAUDRATE
		{
			IRBR300,
			IRBR600,
			IRBR1200,
			IRBR2400,
			IRBR4800,
			IRBR9600,
			IRBR19200
		};

		public class PowerStatus
		{
			public bool charging;
			public int batteryVoltage;
			public int batteryLifePercent;
		};

		bool Disposed = false;
		SerialDevice Device = null;

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (Disposed)
				return;

			if (disposing)
				Device.Dispose();

			Disposed = true;
		}

		public async Task ConnectAsync(DeviceInformation deviceInfo)
		{
			using (CancellationTokenSource cts = new CancellationTokenSource(5000))
				Device = await SerialDevice.FromIdAsync(deviceInfo.Id).AsTask(cts.Token);
		}

		public async Task WriteAsync(byte[] bytes)
		{
			foreach (byte data in bytes)
			{
				if ((data & 0x80) != 0)
					throw new Exception("Invalid data");
			}
			await WriteInternalAsync(bytes);
		}

		public async Task<byte[]> ReadAsync(int numberOfBytesToRead)
		{
			return (await ReadInternalAsync(numberOfBytesToRead));
		}

		public async Task<UInt16> CmdGetVersionAsync()
		{
			await WriteCommandAsync(PROBE_COMMAND.GETVERSION, null);

			byte[] resultData = await ReadResultAsync();
			if (resultData.Length != 2)
				return (0);

			return (UInt16) (((resultData[1] << 8) & 0xff00) | (resultData[0] & 0xff));
		}

		public async Task<String> CmdGetBtVersionAsync()
		{
			await WriteCommandAsync(PROBE_COMMAND.GETBTVERSION, null);
			return (System.Text.Encoding.ASCII.GetString(await ReadResultAsync()));
		}

		public async Task CmdPowerOff()
		{
			await WriteCommandAsync(PROBE_COMMAND.POWEROFF, null);
			await ReadResultAsync();
		}

		public async Task CmdSetLedAsync(bool enable)
		{
			await WriteCommandAsync(PROBE_COMMAND.SETLED, new byte[] { (byte) (enable ? 1 : 0) });
			await ReadResultAsync();
		}

		public async Task CmdIrEnableAsync(bool enable)
		{
			await WriteCommandAsync(PROBE_COMMAND.IRENABLE, new byte[] { (byte)(enable ? 1 : 0) });
			await ReadResultAsync();
		}

		public async Task CmdIrSetBaudRateAsync(IR_BAUDRATE baudRate)
		{
			await WriteCommandAsync(PROBE_COMMAND.IRSETBAUDRATE, new byte[] { (byte)baudRate });
			await ReadResultAsync();
		}

		public async Task<PowerStatus> CmdGetPowerStatusAsync()
		{
			await WriteCommandAsync(PROBE_COMMAND.GETPOWERSTATUS, null);

			byte[] result = await ReadResultAsync();
			if (result.Length != 4)
				throw new Exception("Invalid data");

			PowerStatus status = new PowerStatus();
			status.charging = (result[0] != 0);
			status.batteryVoltage = ((result[2] << 8) & 0xff00) | (result[1] & 0xff);
			status.batteryLifePercent = (result[3] & 0xff);
			return (status);
		}

		private async Task WriteCommandAsync(PROBE_COMMAND command, byte[] parameters)
		{
			byte[] header = new byte[2];
			header[0] = (byte)((1 << 7) | ((int)command << 2) | (int)PACKET_CLASS.COMMAND);
			header[1] = (byte)(2 + ((parameters == null) ? 0 : parameters.Length));
			await WriteInternalAsync(header);

			if (parameters != null)
				await WriteInternalAsync(parameters);
		}

		private async Task<byte[]> ReadResultAsync()
		{
			while (true)
			{
				byte[] packetHeader = (await ReadInternalAsync(2));
				if ((packetHeader[0] & 0x80) == 0)
					continue;

				int packetClass = (packetHeader[0] & 0x03);
				int packetCode = (packetHeader[0] >> 2) & 0x1f;
				int packetBodyLength = packetHeader[1] - 2;

				byte[] packetBody = (packetBodyLength == 0) ? new byte[0] : (await ReadInternalAsync(packetBodyLength));
				if ((PACKET_CLASS)packetClass == PACKET_CLASS.RESULT)
				{
					if ((PROBE_RESULT)packetCode != PROBE_RESULT.SUCCESS)
						return (null);

					return (packetBody);
				}
			}
		}

		private async Task WriteInternalAsync(byte[] bytes)
		{
			if (bytes.Length == 0)
				return;

			using (CancellationTokenSource cts = new CancellationTokenSource(2000))
				await Device.OutputStream.WriteAsync(bytes.AsBuffer()).AsTask(cts.Token);
		}

		private async Task<byte[]> ReadInternalAsync(int numberOfBytesToRead)
		{
			using (CancellationTokenSource cts = new CancellationTokenSource(2000))
			{
				IBuffer buffer = new byte[numberOfBytesToRead].AsBuffer();
				await Device.InputStream.ReadAsync(buffer, buffer.Capacity, InputStreamOptions.None).AsTask(cts.Token);
				return (buffer.ToArray());
			}
		}
	}
}
